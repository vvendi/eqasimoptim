# Modeling the city of Calais : application to bus stops spacing problem

This repository contains the code to create a synthetic population of the city of Calais in France using Eqasim (see eqasim main reference below).

## Configuration

Requirements to run the project :
- Java (used version openjdk 11.0.20.1 2023-08-24)
- Javac (used version 11.0.20.1)
- Maven (used version 3.6.3)
- Osmosis (used version 0.48.3-1)
- Git (used version 3.6.3)
- cmake (used version 3.22.1)
- make (used version 4.3)
- gcc (used version 11.4.0)
- Python (used version 3.9.7)
- Python environment : we recommand to use a conda environment (used version 4.10.3), dependencies can be found in "eqasim-pipeline/environment.yml"
- Update the paths in "eqasim-pipeline/config_base.yml" by replacing "/path/to/project/directory" to the actual path of the project

Optionnal : 
- Taxicab for python if working on another bus line

## Missing Data

Some of the data files cannot be added to the git repo as they are too big.

Files to put to "bdtopo_hdf" :
- From https://geoservices.ign.fr/bdtopo, in the sidebar on the right, under "Téléchargement anciennes éditions", click on "BD TOPO® 2022 GeoPackage Départements" to go to the saved data publications from 2022 and download "BDTOPO\_3-0\_TOUSTHEMES\_GPKG\_LAMB93\_D059\_2022-03-15.7z" and "BDTOPO\_3-0\_TOUSTHEMES\_GPKG\_LAMB93\_D062\_2022-03-15.7z" by clicking on the download links for "Département 59 - Nord" and "Département 62 - Pas-de-Calais"

Files to put to "data/rp_2019" :
- From https://www.insee.fr/fr/statistiques/6544333 download "RP2019\_INDCVI\_csv.zip" by clicking on the csv download link under "Individus localisés au canton-ou-ville"
- From https://www.insee.fr/fr/statistiques/6543200 download "base-ic-evol-struct-pop-2019.zip" by clicking on the xlsx download link under "France hors Mayotte"
- From https://www.insee.fr/fr/statistiques/6456056 download "RP2019\_MOBPRO\_csv.zip" by clicking on the csv download link
- From https://www.insee.fr/fr/statistiques/6456052 download "RP2019\_MOBSCO\_csv.zip" by clicking on the csv download link

Files to put to "data/sirene" :
- From https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/ download "StockEtablissement\_utf8.zip" and "StockUniteLegale\_utf8.zip" by clicking on the download links "Sirene : Fichier StockEtablissement du dd mmmmmm yyyy" and "Sirene : Fichier StockUniteLegale du dd mmmmmm yyyy"
- From https://www.data.gouv.fr/fr/datasets/geolocalisation-des-etablissements-du-repertoire-sirene-pour-les-etudes-statistiques/ download "GeolocalisationEtablissement\_Sirene\_pour\_etudes\_statistiques\_utf8.zip" by clicking on the download link "Sirene : Fichier GeolocalisationEtablissement_Sirene_pour_etudes_statistiques du dd mmmmmm yyyy"

## Other Data files sources
- Adreses database (folder "ban_hdf") : https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/
- Service and facility census (folder "bpe_2021") : https://www.insee.fr/fr/statistiques/3568638
- Zoning registry (folder "codes_2021") : https://www.insee.fr/fr/information/2017499
- National household travel survey (folder "entd_2008") : https://www.statistiques.developpement-durable.gouv.fr/enquete-nationale-transports-et-deplacements-entd-2008
- Income Tax Data (folder "filosofi_2019") : https://www.insee.fr/fr/statistiques/6036907
- GTFS data (folder "gtfs_hdf") : https://transport.data.gouv.fr/datasets/horaires-theoriques-et-temps-reel-du-reseau-sitac-calais-gtfs-gtfs-rt (version might be different)
- IRIS data (folder "iris_2021") : https://geoservices.ign.fr/contoursiris
- OpenStreetMap data (folder "osm_hdf") : initial file from https://download.geofabrik.de/europe/france/nord-pas-de-calais.html and then manually processed

## Run the project

```bash
conda activate eqasim
mkdir build
cd build
cmake ..
make EqasimOptim
./EqasimOptim
```

## Eqasim Main reference

https://github.com/eqasim-org/ile-de-france
> Hörl, S. and M. Balac (2021) [Synthetic population and travel demand for Paris and Île-de-France based on open and publicly available data](https://www.sciencedirect.com/science/article/pii/S0968090X21003016), _Transportation Research Part C_, **130**, 103291.

